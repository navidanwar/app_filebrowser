<?php
    require_once('interface.php');

    class FileBrowser implements __FileBrowser {
        
        private $rootPath;
        private $currentPath;
        private $extensionFilter;


        public function __construct($rootPath, $currentPath = null, array $extensionFilter = array()){

            $this->rootPath         = $rootPath;
            $this->currentPath      = $currentPath;
            $this->extensionFilter  = $extensionFilter;
        }

        /**
         * Set private root path
         */
        public function SetRootPath($rootPath){
            $this->rootPath = $rootPath;
        }

        /**
         * Set private current path
         */
        public function SetCurrentPath($currentPath){
            $this->currentPath = $currentPath;
        }

        /**
         * Set private extension filter
         */
        public function SetExtensionFilter(array $extensionFilter){
            $this->extensionFilter = $extensionFilter;
        }

        /**
         * Get files using currently-defined object properties
         * @return array Array of files within the current directory
         */
        public function Get(){
            $rPath = $this->getRootPath();
            $cPath = $this->getCurrentPath();
            $files = array();

            if (is_dir($cPath)) {
                if ($dirHan = opendir($cPath)) {
                    while (($file = readdir($dirHan)) !== false) {
                        if($file != '.' && $file != '..'){
                           
                            if(filetype($cPath.$file) != 'dir'){
                                $f = new SplFileInfo($file);

                                if(!in_array($f->getExtension(), $this->getExtensionFilter()))
                                    $files[] = ucwords($file);
                            }
                            else{
                                $files[] = ucwords($file);
                            }
                        }
                    }
                    closedir($dirHan);
                }
            }
            
            return $files;
        }

        /**
         * Get RootPath
         * @return type
         */
        public function getRootPath(){
            return $this->rootPath;
        }

        /**
         * Get CurrentPath
         * @return type
         */
        public function getCurrentPath(){
            return $this->currentPath;
        }

        /**
         * Get ExtensionFilter
         * @return type
         */
        public function getExtensionFilter()
        {
            return $this->extensionFilter;
        }
    }