<?php

    /**
     * Disclaimer: This application is developed by Naveed Anwar on request from 
     * Eddy Kim (HaysIT) as a techincal test to proceed further for the job process
     * with the company.
     * 
     * NOTE: This application requires 
     *      - Webserver (WAMP/APAChE)
     *      - PHP 5.5
     *      - Internet Connectivity for using BootStrap 3 framework
     */
    require_once('filebrowser.php');

    /*
     * Add your filebrowser definition code here
     */
       
    $folder     = isset($_REQUEST['folder'])    ? $_REQUEST['folder']   : "";
    $filename   = isset($_REQUEST['filename'])  ? $_REQUEST['filename'] : "";
    
    /**
     * Define the directory path in the following ways
     * On Windows,  $rootPath = "D:/example-2";
     * On Linux,    $rootPath = "/var/www/vhosts";
     * 
     * NOTE: This application is made and tested on Windows 10 using WAMP server 2.5 
     * 
     * For Quick install, just place the application under
     * <dirve>:/wamp/www/  
     * folder and then it has an example folder and will by default. No need to set path.
     * Otherwise set the path as described above to any directo
     */
    $rootPath = getcwd()."/example"; //Default path for the directory in the application root directory
    
    /**
     * Set the files to be excluded from the directory listing
     * e.g. Below the files with txt and pdf extension will not be displayed in the directory listing
     */
    $extensionFilter = array('txt', 'pdf'); //Do not show these type of files
    
    
    /**
     * Determining the root directory so that the application will not browse level up of it
     * e.g. In the following path D:\Example\Notes
     * Application will calculate "Notes" as the root directory and will display contents 
     * from it and from all directories beneath it
     */
    $rootDir = "";
    $start = strripos($rootPath, '/');
    $end   = strlen($rootPath);
    if ($start !== false) $rootDir = substr ($rootPath, $start+1, $end+1);
    //echo "rootDir = $rootDir <br />";
    
    $rootPath .= "/";

    /**
     * Determining the current Directory
     */
    $currDir = "";
    if($currDir != "" && $folder != "") $currDir = $currDir . $folder;
    else                                $currDir = $folder;
    //echo "currDir        = $currDir <br />";
        
    
    /**
     * Determining the Parent Directory Path to move level up
     */
    $parentDir = "";
    if($parentDir != '/'){
        $pos = strripos($folder, '/');
        if ($pos !== false) $parentDir = substr ($folder, 0, $pos);
    }
    
    
    /**
     * When the user clicks the file to open in the directory then the below
     * code will send the file to the browser for download.
     */
    if($filename != ""){
        $filePath = $rootPath.$filename;
        if(file_exists($filePath)){
            header('Content-Description: File Transfer');
            header('Content-Disposition: attachment; filename="'.basename($filePath).'"');
            header('Expires: 0');
            header('Cache-Control: must-revalidate');
            header('Content-Length: ' . filesize($filePath));
            ob_clean();
            flush();
            readfile($filePath);
            exit;
        }
    }
    
    
    /**
     * Determining the currentPath for using it in the FileBrowser Object
     */
    if ($folder != "") {
        $currentPath = $rootPath.$folder."/";
    } else {
        $currentPath = $rootPath;
    }
    
    
//    echo "parentDir = $parentDir <br />";
//    echo "folder Path   = $folder <br />";
//    echo "Current Path = ".$currentPath."<br />";
//    echo "parent path = " .$parentDir ."<br />";
    
    /**
     * Creating the FileBrowser Object
     */
    $fileBrowserObj = new FileBrowser($rootPath, $currentPath, $extensionFilter);
     
    
//    echo "root path = ". $fileBrowserObj->getRootPath() . "<br />";
//    echo "current path = ". $fileBrowserObj->getCurrentPath() . "<br />";
//    echo "extension filters = "; print_r($fileBrowserObj->getExtensionFilter()); echo "<br />";
    
    
    /**
     * Fetching the Directory Files and Folders
     */
    $directoryFiles = $fileBrowserObj->Get();
    
    
?>
<!doctype html>
<html lang="en">
    <head>
        <title>File browser</title>
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" integrity="sha512-dTfge/zgoMYpP7QbHy4gWMEGsbsdZeCXz7irItjcC3sPUFtf0kuFbDz/ixG7ArTxmDjLXDmezHubeNikyKGVyQ==" crossorigin="anonymous">
    </head>
    <body>
        <div class="container">
            <h2>
                <a class="label label-primary" href="?folder=<?php echo $parentDir ?>"> <span class="glyphicon glyphicon-arrow-left"></span> Parent Directory </a>
            </h2>
            <div class="panel panel-primary">
                <div class="panel-body" style="background-color: #337ab7; color: #ffffff;">
                    <h3 class="panel-title"> Directory Listing </h3>
                </div>
                <div class="panel-body">
                    <?php
                        /**
                         * Displaying files and folders in the directory
                         */
                        if($directoryFiles){
                            foreach($directoryFiles as $file){
                                if(filetype($currentPath.$file) == 'dir'){
                                    echo "<a href=?folder=$currDir/$file><span class='glyphicon glyphicon-folder-close'></span> $file</a> <br />";
                                }
                                else{
                                    if($currDir == ""){
                                        echo "<a href=?filename=$file><span class='glyphicon glyphicon-file'></span> $file</a> <br />";
                                    }
                                    else{
                                        echo "<a href=?filename=$currDir/$file><span class='glyphicon glyphicon-file'></span> $file</a> <br />";
                                    }
                                }   
                            }
                        }
                        else{
                            echo "No files found in the directory <br />";
                        }
                    ?>
                </div>
            <div>
        </div>
    </body>
</html>